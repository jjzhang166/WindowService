﻿using System;
using System.ComponentModel;
using System.Threading;
using System.IO;

namespace WindowServices.Common
{
    [Description("Test Service1.")]
    [DisplayName(@"EmptyService1")]
    [System.Runtime.InteropServices.Guid("877FD1A6-9D73-4835-8514-402C2ACBE739")]
    [Serializable]
    public class EmptyService:Interface.IService
    {
        #region IService Members
        Interface.ServiceEntity _se;
        public void Initialize(Interface.ServiceEntity serviceEntity)
        {
            _se = serviceEntity;
        }

        bool _isRunning;
        public void Start()
        {
            _isRunning = true;
            while (_isRunning)
            {
                try
                {
                    File.AppendAllText("D:\\2.txt", string.Format("ID={0},Name={1},Type={2},Version={3}", _se.Id, _se.Name, _se.Type, _se.Version));
                }
                catch
                {
                }
                Thread.Sleep(5000);
            }
        }

        public void Stop()
        {
            _isRunning = false;
        }

        public void Pause()
        {
            
        }

        #endregion
    }
}
