﻿namespace WindowServices.Interface
{
    /// <summary>
    /// 插件服务接口
    /// </summary>
    public interface IService
    {
        /// <summary>
        /// 初始化插件
        /// </summary>
        /// <param name="serviceEntity">插件配置</param>
        void Initialize(ServiceEntity serviceEntity);

        /// <summary>
        /// 启动服务
        /// </summary>
        void Start();

        /// <summary>
        /// 停止服务
        /// </summary>
        void Stop();

        /// <summary>
        /// 暂停服务
        /// </summary>
        void Pause();
    }
}