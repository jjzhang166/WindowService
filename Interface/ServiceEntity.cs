﻿using System;

namespace WindowServices.Interface
{
    /// <summary>
    /// 服务基础信息
    /// </summary>
    [Serializable]
    public class ServiceEntity
    {
        /// <summary>
        /// 服务名称
        /// </summary>
        public string Name { get; set; }


        /// <summary>
        /// 程序实现
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 服务版本
        /// </summary>
        public Version Version { get; set; }

        /// <summary>
        /// 程序编号
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 程序基础路径
        /// </summary>
        public string BasePath
        {
            get
            {
                return string.Format("{0}Plug\\{1}\\{2}", AppDomain.CurrentDomain.BaseDirectory, Id, Version);
            }
        }

        /// <summary>
        /// 插件压缩文件夹
        /// </summary>
        public string ZipPath
        {
            get
            {
                return string.Format("{0}Zip", AppDomain.CurrentDomain.BaseDirectory);
            }
        }
        /// <summary>
        /// 插件压缩文件路径
        /// </summary>
        public string ZipFileName
        {
            get
            {
                return string.Format("{0}Zip\\{1}_{2}.Zip", AppDomain.CurrentDomain.BaseDirectory, Id, Version);
            }
        }
    }

   
}
