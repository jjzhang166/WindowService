using System.Collections.Generic;
using WindowServices.Server.Interface;
using WindowServices.Service.Command.Task;

namespace WindowServices.Service.Command
{
    internal sealed class UnInstallCommand : CommandBase
    {
        public UnInstallCommand(Dictionary<string, ServiceHelper> services, ServiceEntityEx se)
            : base(new List<ITask>
                       {
                           new UnLoadServiceTask(services, se, 1), 
                           new DeleteServiceDirTask(se, 3),
                           new DeleteConfigTask(se,3),
                       })
        {
        }
    }
}