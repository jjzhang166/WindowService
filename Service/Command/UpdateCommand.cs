using System.Collections.Generic;
using WindowServices.Server.Interface;
using WindowServices.Service.Command.Task;

namespace WindowServices.Service.Command
{
    internal sealed class UpdateCommand : CommandBase
    {
        public UpdateCommand(Dictionary<string, ServiceHelper> services, ServiceEntityEx se, ServiceEntityEx old)
            : base(new List<ITask>
                       {
                           new UnLoadServiceTask(services,old,1),
                           new UnZipFileTask(se, 2), 
                           new LoadServiceTask(services, se, 3), 
                           new SaveConfigTask(se, 4),
                           new DeleteServiceDirTask(old,5),
                           new DeleteConfigTask(old,6),
                       })
        {
        }
    }
}