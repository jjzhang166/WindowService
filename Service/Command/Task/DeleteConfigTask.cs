using WindowServices.Server.Interface;

namespace WindowServices.Service.Command.Task
{
    internal sealed class DeleteConfigTask : TaskBase
    {
        public DeleteConfigTask(ServiceEntityEx se, int order)
            : base(se, order)
        {
        }

        public DeleteConfigTask(ServiceEntityEx se)
            : base(se)
        {
        }

        protected override void RollBack(ServiceEntityEx se)
        {
            Common.ServiceHelper.AddServiceEntity(se);
        }

        protected override void Execute(ServiceEntityEx se)
        {
            Common.ServiceHelper.DelServiceEntity(se.Id, se.Version);
        }
    }
}