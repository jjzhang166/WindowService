using System.Collections.Generic;
using WindowServices.Server.Interface;

namespace WindowServices.Service.Command.Task
{
    internal sealed class UnLoadServiceTask : TaskBase
    {
        private readonly Dictionary<string, ServiceHelper> _services;
        public UnLoadServiceTask(Dictionary<string, ServiceHelper> services, ServiceEntityEx se)
            : base(se)
        {
            _services = services;
        }

        public UnLoadServiceTask(Dictionary<string, ServiceHelper> services, ServiceEntityEx se, int order)
            : base(se, order)
        {
            _services = services;
        }
        protected override void RollBack(ServiceEntityEx se)
        {
            Utility.LoadService(se);
        }

        protected override void Execute(ServiceEntityEx se)
        {
            if (!_services.ContainsKey(se.Key))
            {
                return;
            }

            var sh = _services[se.Key];
            _services.Remove(se.Key);
            Utility.UNload(se.Key, sh);
        }
    }
}