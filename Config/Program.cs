﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WindowServices.Config.Properties;

namespace WindowServices.Config
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (System.Configuration.ConfigurationManager.AppSettings["Language"] != null)
            {
                Resources.Culture = new CultureInfo(System.Configuration.ConfigurationManager.AppSettings["Language"]);
            }

            Application.Run(new MainForm());
        }
    }
}
