﻿using System.Collections.Generic;
using System.ServiceModel;
using WindowServices.Server.Interface;

namespace WindowServices.Config
{
    public class ServerContext
    {
        public ServerContext()
        {
            if (string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["ServiceType"]))
            {
                _localServer = ChannelFactory<IServer>.CreateChannel(new BasicHttpBinding(),
                                                                                 new EndpointAddress("http://localhost:8089"));
            }
            else
            {
                string[] strs = System.Configuration.ConfigurationManager.AppSettings["ServiceType"].Split(",".ToCharArray(), 2);
                _localServer = (IServer)System.Reflection.Assembly.Load(strs[1]).CreateInstance(strs[0]);
            }

        }
        private readonly IServer _localServer;// new LocalServer();
        public void Start()
        {
            _localServer.Start();
        }

        public void Stop()
        {
            _localServer.Stop();
        }

        public void SendCustomCommand(int command, ServiceEntityEx serviceEntity, byte[] fileContent)
        {
            _localServer.SendCustomCommand(command, serviceEntity, fileContent);

        }

        public ServerState GetServiceState()
        {
            return _localServer.GetServiceState();
        }

        public void Pause()
        {
            _localServer.Pause();
        }

        public void Continue()
        {
            _localServer.Continue();
        }

        public List<ServiceEntityEx> GetServiceList()
        {
            return _localServer.GetServiceList();
        }


        public void WaitForCommplete()
        {
            _localServer.WaitForCommplete();
        }

        public void WaitForCommplete(int timeOut)
        {
            _localServer.WaitForCommplete(timeOut);
        }
    }
}
