﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using WindowServices.Config.Properties;
using WindowServices.Server.Interface;

namespace WindowServices.Config
{
    public partial class ServiceFrom : Form
    {
        private ServerContext _serverProxy;
        public ServiceFrom()
        {
            InitializeComponent();
        }

        public ServiceFrom(ServerContext serverProxy)
            : this()
        {
            _serverProxy = serverProxy;
        }

        string tmpPath = string.Format("{0}{1}\\", AppDomain.CurrentDomain.BaseDirectory, "temp");

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() != DialogResult.OK)
            {
                return;
            }


            try
            {
                if (Directory.Exists(tmpPath))
                {
                    Directory.Delete(tmpPath, true);
                }
                Directory.CreateDirectory(tmpPath);
                Common.ZipHelper.UnZip(openFileDialog1.FileName, tmpPath, null);
            }
            catch (Exception ee)
            {
                showerror(ee.Message);
                return;
            }

            if (!File.Exists(string.Format("{0}{1}\\Main.xml", AppDomain.CurrentDomain.BaseDirectory, "temp")))
            {
                showerror(string.Format(Resources.FileFormatError, "Main.xml","Xml"));
                return;
            }

            _se = new ServiceEntityEx();
            try
            {
                using (Common.ConfigHelper helper = new Common.ConfigHelper(string.Format("{0}{1}\\Main.xml", AppDomain.CurrentDomain.BaseDirectory, "temp")))
                {
                    _se.Id = new Guid(helper.GetInnerText("/Plug/ID"));
                    _se.Name = helper.GetInnerText("/Plug/Name");
                    _se.Type = helper.GetInnerText("/Plug/Type");
                    _se.Version = new Version(helper.GetInnerText("/Plug/Version"));
                }
            }
            catch (Exception ee)
            {
                showerror(ee.Message);
                return;
            }

            if (!PlugHelper.CheckType(_se.Type))
            {
                showerror(string.Format(Resources.FileFormatError, "Main.xml", "Xml"));
                return;
            }

            txtID.Text = _se.Id.ToString();
            txtName.Text = _se.Name;
            txtType.Text = _se.Type;
            txtVersion.Text = _se.Version.ToString();

            textBox1.Text = openFileDialog1.FileName;
        }

        private void showerror(string message)
        {
            MessageBox.Show(message, Resources.Tips, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void chkInheritance_CheckedChanged(object sender, EventArgs e)
        {
            gbUserInfo.Enabled = !chkInheritance.Checked;
        }

        ServiceEntityEx _se = new ServiceEntityEx();

        private void btnSave_Click(object sender, EventArgs e)
        {
            _se.Inheritance = chkInheritance.Checked;
            if (!_se.Inheritance)
            {
                _se.UserInfo.Domain = txtDomain.Text;
                _se.UserInfo.UserId = txtUserID.Text;
                _se.UserInfo.PassWord = txtPassword.Text;
            }
            var list = _serverProxy.GetServiceList().Where(c => c.Id == _se.Id).ToList();

            if (list.Any(se1 => se1.Version.CompareTo(_se.Version) >= 0))
            {
                showerror(Resources.ServiceIsExistsNewVerison);
                return;
            }


            try
            {
                _serverProxy.SendCustomCommand(list.Count > 0 ? (int)ServiceCommand.Update : (int)ServiceCommand.Add, _se, File.ReadAllBytes(openFileDialog1.FileName));
                _serverProxy.WaitForCommplete(300);
            }
            catch (Exception ee)
            {
                showerror(ee.Message);
                return;
            }

            this.DialogResult = DialogResult.OK;
        }

        private void ServiceFrom_Load(object sender, EventArgs e)
        {
            FreshUi.FreshUIControl(this);
        }
    }
}
